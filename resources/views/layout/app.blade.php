<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" href="{{ asset('') }}">
    <link href="{{ asset('assets/login/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/login/css/all.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('assets/login/css/typicons.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{ asset('assets/login/css/themify-icons.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{ asset('assets/login/css/toastr.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('assets/login/css/login.css') }}" rel=stylesheet type="text/css"/>
    <link href="{{ asset('assets/fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css"/>

    <style>
        .bg-img-hero {
            height: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: top center;
            background-image: url({{ asset('assets/login/img/bg.jpg') }});
        }
    </style>

</head>
<body class="bg-white body-bg">
    <div class="bg-img-hero position-fixed top-0 right-0 left-0 ">
    </div>
    <main class="register-content">
        @yield('content')
    </main>

    <script src="{{ asset('assets/login/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/login/js/bootstrap.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('assets/login/js/toastr.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="{{ asset('assets/login/js/sidebar.js') }}" type="text/javascript"></script> --}}
    <script src="{{ asset('assets/login/js/hideShowPassword.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var info = $('table tbody tr');
            info.click(function() {
                var username = $(this).children().first().text();
                var password = $(this).children().first().next().text();
                var user_role = $(this).attr('data-role');

                $("input[type=username]").val(username);
                $("input[type=password]").val(password);
                $('select option[value='+user_role+']').attr("selected", "selected");
            });
        });
    </script>
    {{-- <script defer="defer" src="{{ asset('assets/login/js/beacon.js') }}" data-cf-beacon="{&quot;si&quot;:10,&quot;version&quot;:&quot;2021.2.0&quot;,&quot;rayId&quot;:&quot;6279e7f7de1b22c0&quot;}"></script> --}}
</body>
</html>
