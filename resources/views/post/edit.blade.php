@extends('layout.master')

@push('textcss')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css') }}"/>
    <link href="{{asset('adminlte/plugins/sweetalert2/sweetalert2.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/plugins/croppie/croppie.css') }}"/>
    <style>
        .select2-container--default
        .select2-selection--multiple
        .select2-selection__rendered
        .select2-selection__choice {
            color: #444;
        }
    </style>
@endpush

@section('content')
<div class="container">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Artikel</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <!-- form start -->
                    <form class="form-horizontal" action="/admin/artikel/{{$artikel->id}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="title">Judul</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="title" name="title" placeholder="Masukkan Judul" value="{{$artikel->title}}">
                                @error('title')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="slug">Slug</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="slug" name="slug" placeholder="Masukkan Intro" value="{{$artikel->slug}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="content">Konten</label>
                            <div class="col-sm-10">
                                <textarea id="content" name="content" class="form-control" cols="30" rows="10">{{$artikel->content}}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="category_id">Kategori</label>
                            <div class="col-sm-10">
                                <select class="form-control js-example-basic-single" name="category_id">
                                    @foreach($listcategory as $key=>$value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="tag_id">Tags</label>
                            <div class="col-sm-10">
                                <select class="form-control js-example-basic-multiple" name="tag_id[]" multiple="multiple">
                                    @foreach($listtag as $key=>$tags_val)
                                        <option value="{{$tags_val->id}}"
                                        @foreach($listpoststags as $key=>$posttags_val)
                                            @if ($tags_val->id == $posttags_val->tag_id)
                                                selected="selected"
                                            @endif
                                        @endforeach
                                        >{{$tags_val->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{-- start upload foto --}}
                        <div class="form-group row">
                            <div class="row col-sm-12">
                                <label class="col-sm-2 col-form-label" for="email">Gambar</label><br>
                                <input type="hidden" name="foto">
                                <div class="col-sm-10">
                                    <label class="btn btn-default" style="width: 100%">
                                        Pilih File
                                        <input type="file" name="__files[]" id="upload" value="Choose a file" accept="image/x-png,image/gif,image/jpeg" hidden/>
                                    </label>
                                </div>
                            </div>
                            <div class="row col-sm-12">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <div class="col-md-12 text-center">
                                        <img class="img img-crop"
                                        width="50%" src="{{ asset('images/'.$artikel->image_post) }}">
                                    </div>
                                    <div class="col-md-12 display-crop">
                                        <div class="upload-demo-wrap">
                                            <div id="upload-demo"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center display-crop hidden">
                                        <button type="button" class="btn btn-default crop">Crop</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- end upload foto --}}
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.card -->

        </div>
        <!--/.col (left) -->
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('/adminlte/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
<script src="{{asset('assets/plugins/croppie/croppie.min.js')}}"></script>

<!-- Page specific script -->
<script>
    $(function() {
        // Summernote
        $('#content').summernote({
                height: 300,   //set editable area's height
            })
    });

    $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.js-example-basic-multiple').select2();
    });

    var $croppie
    $(document).on('change','#upload', function () {
        $('.img-crop, .display-crop').addClass('hidden')
        $('.display-crop').removeClass('hidden')

        if ($('.cr-image').length == 1) {
            $('#upload-demo').croppie('destroy');
        }
        $croppie = $('#upload-demo').croppie({
            viewport: {
                width: 500,//0,3578
                height: 179,
            },
            mouseWheelZoom: true,
            boundary: { width: 500, height: 300 },
            enableExif: true,
            showZoomer: false
        })
        readFile(this);

    });

    $('.crop').on('click', function (ev) {
        $croppie.croppie('result', {
            type: 'base64',
            size: {width: 1048,height: 375},
            quality:0.9,
            format : 'jpeg'
        }).then(function (resp) {
            popupResult({
                src: resp
            });
        });
    });
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $croppie.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal.fire("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    function popupResult(result) {
        var html;
        let img = result.src;

        if (result.src) {
            html = '<img width="100%" src="' + result.src + '" />';
        }
        Swal.fire({
            title: '',
            html: html,
            allowOutsideClick: false,
            width: 1048,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Gunakan Foto!'
        }).then((result) => {
            if (result.value) {
                $('input[name=foto]').val(img)
                $('#upload-demo').croppie('destroy');
                $('.img-crop').attr('src', img).removeClass('hidden');
                $('.display-crop').addClass('hidden')
            }
            else{
                $('input[name=foto]').val('')

            }
        });
    }
</script>
@endpush
