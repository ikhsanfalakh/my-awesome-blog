@extends('layout.master')

@section('content')
<section class="content">
    <!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Artikel</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <h4>{{$artikel->title}}</h4>
        <p>Created date : {{$artikel->created_at}} </p>
        <p>Category : {{$artikel->categoryname}}</p>
        <hr/>
        <p>{!! $artikel->content !!}</p>
        <p>Tags :</p>
        <p>
        @forelse ($listtag as $tags)
    <li>{{ $tags->name }}</li>
@empty
    Tidak Ada Tags
@endforelse
        </p>
    </div>
    <!-- /.card-body -->
</div>
</section>
@endsection
