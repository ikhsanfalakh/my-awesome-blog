@extends('layout.master')

@section('content')

<section class="content">
    <!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Category</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <h2>Tampilkan Category ID {{$category->id}}</h2>
        <h4>{{$category->name}}</h4>
    </div>
    <!-- /.card-body -->
</div>
</section>
@endsection
