@extends('layout.app')

@section('content')
<div class="container py-sm-7" style="padding-top:5rem">
    <div class="row justify-content-center">
        <div class="col-md-7 col-lg-5">
            <div class="form-card mb-2">
                <div class="form-card_body pt-4">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="text-center">
                            <div class="mb-4">
                                <h1 class="display-5 mt-0 font-weight-semi-bold">{{ __('Login') }}
                                    <i class="fa fa-user-lock"></i>
                                </h1>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="input-label" for="signinSrEmail">{{ __('E-Mail Address') }}</label>
                            <div class="position-relative">
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    id="username" name="email" value="{{ old('email') }}" autocomplete="email"
                                    autofocus tabindex="1" placeholder="user@email.com" aria-label="email@address.com"
                                    required="" data-msg="Please enter a valid email address.">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="input-label d-flex justify-content-between align-items-center" for="signupSrPassword" tabindex="-1">
                                <label class="input-label" for="password">Password</label>
                            </label>
                            <div class="position-relative">
                                <input type="password" class="form-control password @error('password') is-invalid @enderror" id="pass" name="password" placeholder="Password *" tabindex="2">
                                <i class="fa fa-eye-slash"></i>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <button type="submit" class="btn btn-lg btn-block btn-success" tabindex="3">{{ __('Login') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
