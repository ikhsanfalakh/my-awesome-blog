<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin My Awesome Blog</title>
    <link rel="shortcut icon" href="{{ asset('') }}">
    <link href="{{ asset('assets/login/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/login/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/login/css/typicons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/login/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/login/css/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/login/css/login.css') }}" rel=stylesheet type="text/css"/>
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .bg-img-hero {
            height: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: top center;
            background-image: url({{ asset('assets/login/img/bg.jpg') }});
       }
    </style>
</head>

<body class="bg-white body-bg">
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Admin login area start-->

<main class="register-content">
    <div class="bg-img-hero position-fixed top-0 right-0 left-0 ">
    </div>

    <div class="container py-5 py-sm-7">
        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <a class="d-flex justify-content-center mb-3 pharmacare-logo" href="javascript:void(0)">
            <img class="z-index-2" src="<?php if (isset($Web_settings[0]['logo'])) {
               echo $Web_settings[0]['logo']; }?>" alt="Image Description">
        </a>
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <div class="form-card mb-2">
                    <div class="form-card_body">
                        <?php echo form_open('Authentication/do_login', array('id' => 'login',)) ?>
                            <input type="hidden" name="app_csrf" value="6b4d58e164e19b7ee9cf5f13d0aba3f0">
                            <div class="text-center">
                                <div class="mb-5">
                                    <h1 class="display-4 mt-0 font-weight-semi-bold">Sign in</h1>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="input-label" for="signinSrEmail">Your Email</label>
                                <input type="email" class="form-control" id="username" name="username" tabindex="1" placeholder="Email@example.com" aria-label="email@address.com" required="" data-msg="Please enter a valid email address.">
                            </div>

                            <div class="form-group">
                                <label class="input-label d-flex justify-content-between align-items-center" for="signupSrPassword" tabindex="-1">
                                    <label class="input-label" for="password">Password</label>
                                    <a href="#" data-toggle="modal" data-target="#passwordrecoverymodal" class="font-weight-500" tabindex="-1">Forgot password ?</a>
                                </label>
                                <div class="position-relative">
                                    <input type="password" class="form-control password" id="pass" name="password" placeholder="Password *" tabindex="2">
                                    <i class="fa fa-eye-slash"></i>
                                </div>
                            </div>

                            <?php if ($Web_settings[0]['captcha'] == 0 && $Web_settings[0]['site_key'] != null && $Web_settings[0]['secret_key'] != null) { ?>
                                <div class="g-recaptcha" data-sitekey="<?php
                                if (isset($Web_settings[0]['site_key'])) {
                                    echo html_escape($Web_settings[0]['site_key']);
                                }
                                ?>">
                                </div>
                            <?php } ?>

                            <button type="submit" class="btn btn-lg btn-block btn-success" tabindex="3"><?php echo display('login') ?></button>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="passwordrecoverymodal" tabindex="-1" role="dialog" aria-labelledby="recoverylabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="recoverylabel">Passwor Recovery</h5>
                    </div>
                    <form action="https://pharmacyv5.bdtask.com/pharmacare-9.4_demo/recovery_mail" class="form-vertical" id="passrecoveryform" method="post" accept-charset="utf-8">
                        <input type="hidden" name="app_csrf" value="6b4d58e164e19b7ee9cf5f13d0aba3f0">
                        <div class="modal-body">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label for="email" class="col-lg-3 col-form-label">Email <i class="text-danger">*</i></label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="rec_email" id="rec_email" type="text" placeholder="Email" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal_close">Close</button>
                            <input type="submit" id="submit_recovery" class="btn btn-success" value="Send">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="{{ asset('assets/login/js/jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/login/js/bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/login/js/toastr.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/login/js/sidebar.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/login/js/hideShowPassword.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var info = $('table tbody tr');
        info.click(function() {
            var username = $(this).children().first().text();
            var password = $(this).children().first().next().text();
            var user_role = $(this).attr('data-role');

            $("input[type=username]").val(username);
            $("input[type=password]").val(password);
            $('select option[value='+user_role+']').attr("selected", "selected");
        });
    });
</script>
<script defer="defer" src="{{ asset('assets/login/js/beacon.js') }}" data-cf-beacon="{&quot;si&quot;:10,&quot;version&quot;:&quot;2021.2.0&quot;,&quot;rayId&quot;:&quot;6279e7f7de1b22c0&quot;}"></script>
<!--
<div id="toTop" class="btn-top" style="display: none;">
    <i class="typcn typcn-arrow-up fs-21"></i>
</div>
 -->
</body>
</html>
