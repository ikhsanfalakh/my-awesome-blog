
@extends('layout.master')

@push('textcss')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/croppie/croppie.css') }}" />
    <link href="{{asset('adminlte/plugins/sweetalert2/sweetalert2.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/plugins/croppie/croppie.css') }}"/>

@endpush

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Profile</h3>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
        </div>
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{ asset('images/'.$user->url_foto) }}"
                     alt="User profile picture">
              </div>

              <h2 class="profile-username text-center">{{$user->name}}</h2>
              <hr>
              <strong>Intro</strong>
              <br>
              {!!$user->intro!!}

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Sosial Media</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                {{-- untuk Intro --}}
                <i class="fab fa-twitter fa-fw"></i><a href="{{$user->url_twitter}}" target="_blank">{{$user->url_twitter}}</a>
                <hr>
                <i class="fab fa-linkedin fa-fw"></i><a href="{{$user->url_linkedin}}" target="_blank">{{$user->url_linkedin}}</a>
                <hr>
                <i class="fab fa-facebook fa-fw"></i><a href="{{$user->url_facebook}}" target="_blank">{{$user->url_facebook}}</a>
                <hr>
                <i class="fab fa-instagram fa-fw"></i><a href="{{$user->url_instagram}}" target="_blank">{{$user->url_instagram}}</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">About Me</a></li>
                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Setting</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="active tab-pane" id="activity">
                  <!-- Post -->
                  <div class="post">
                    <!-- /.user-block -->
                    {!!$user->desc!!}
                  </div>
                  <!-- /.post -->
                </div>

                <div class="tab-pane" id="settings">
                    <form class="form-horizontal" action="/admin/profile/{{$user->id}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="name">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama Lengkap" value="{{$user->name}}">
                                @error('name')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="email">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Alamat Email" value="{{$user->email}}">
                                @error('email')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="intro">Intro</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="intro" name="intro" placeholder="Masukkan Intro" value="{{$user->intro}}">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="url_instagram">URL Instagram</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="url_instagram" name="url_instagram" placeholder="Masukkan URL Instagram" value="{{$user->url_instagram}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="url_facebook">URL Facebook</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="url_facebook" name="url_facebook" placeholder="Masukkan URL Facebook" value="{{$user->url_facebook}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="url_website">URL Website</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="url_website" name="url_website" placeholder="Masukkan URL Website" value="{{$user->url_website}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="url_twitter">URL Twitter</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="url_twitter" name="url_twitter" placeholder="Masukkan URL Twitter" value="{{$user->url_twitter}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="url_linkedin">URL LinkedIn</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="url_linkedin" name="url_linkedin" placeholder="Masukkan URL LinkedIn" value="{{$user->url_linkedin}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="desc">About Me</label>
                            <div class="col-sm-10">
                                <textarea id="desc" name="desc" class="form-control" cols="30" rows="10">{{$user->desc}}</textarea>
                            </div>
                        </div>
                        {{-- start upload foto --}}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="email">Foto</label><br>
                            <input type="hidden" name="foto">
                            <label class="btn btn-default col-sm-10">
                                Pilih File
                                <input type="file" name="__files[]" id="upload" value="Choose a file" accept="image/x-png,image/gif,image/jpeg"/>
                            </label>
                            <div class="col-md-12 text-center">
                                <br>
                                <img class="img img-crop"
                                width="80%" src="{{ asset('images/'.$user->url_foto) }}">
                            </div>
                            <div class="col-md-12 display-crop">
                                <div class="upload-demo-wrap">
                                    <div id="upload-demo"></div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center display-crop hidden">
                                <button type="button" class="btn btn-default crop">Crop</button>
                            </div>
                        </div>
                        {{-- end upload foto --}}
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  @endsection

  @push('scripts')
  <script src="{{asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
  <script src="{{asset('adminlte/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
  <script src="{{asset('assets/plugins/croppie/croppie.min.js')}}"></script>

  <script>
        $(function() {
            // Summernote
            $('#desc').summernote({
                    height: 300,   //set editable area's height
                })
        });

    var $croppie
    $(document).on('change','#upload', function () {
        $('.img-crop, .display-crop').addClass('hidden')
        $('.display-crop').removeClass('hidden')

        if ($('.cr-image').length == 1) {
            $('#upload-demo').croppie('destroy');
        }
        $croppie = $('#upload-demo').croppie({
            viewport: {
                width: 200,//0,3578
                height: 200,
                type: 'circle'
            },
            mouseWheelZoom: true,
            boundary: { width: 500, height: 300 },
            enableExif: true,
            showZoomer: false
        })
        readFile(this);

    });

    $('.crop').on('click', function (ev) {
        $croppie.croppie('result', {
            type: 'base64',
            size: {width: 200,height: 200},
            quality:0.9,
            format : 'jpeg'
        }).then(function (resp) {
            popupResult({
                src: resp
            });
        });
    });
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $croppie.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal.fire("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    function popupResult(result) {
        var html;
        let img = result.src;

        if (result.src) {
            html = '<img width="100%" src="' + result.src + '" />';
        }
        Swal.fire({
            title: '',
            html: html,
            allowOutsideClick: false,
            width: 800,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Gunakan Foto!'
        }).then((result) => {
            if (result.value) {
                $('input[name=foto]').val(img)
                $('#upload-demo').croppie('destroy');
                $('.img-crop').attr('src', img).removeClass('hidden');
                $('.display-crop').addClass('hidden')
            }
            else{
                $('input[name=foto]').val('')

            }
        });
    }
  </script>
  @endpush
