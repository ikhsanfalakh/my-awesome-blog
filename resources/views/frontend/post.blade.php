@extends('frontend.master.html')
@section('content')
@isset($dtContent->title)
@php
            $seconds_ago = (time() - strtotime($dtContent->created_at));
            $teksWaktu="Seen less than a minute ago";
            if ($seconds_ago >= 31536000) {
                $teksWaktu=intval($seconds_ago / 31536000) . " years ago";
            } elseif ($seconds_ago >= 2419200) {
                $teksWaktu=intval($seconds_ago / 2419200) . " months ago";
            } elseif ($seconds_ago >= 86400) {
                $teksWaktu=intval($seconds_ago / 86400) . " days ago";
            } elseif ($seconds_ago >= 3600) {
                $teksWaktu=intval($seconds_ago / 3600) . " hours ago";
            } elseif ($seconds_ago >= 60) {
                $teksWaktu=intval($seconds_ago / 60) . " minutes ago";
            }
            @endphp
<article class="blog-post px-3 py-5 p-md-5">
		    <div class="container">
			    <header class="blog-post-header">
				    <h2 class="title mb-2">{{$dtContent->title}}</h2>
				    <div class="meta mb-3"><span class="date">Published {{$teksWaktu}}</span><span class="time">{{$dtContent->view}} view</span><span class="time">Category {{$dtContent->name}}</span></div>
			    </header>
			    
			    <div class="blog-post-body">
				    <figure class="blog-banner">
				        <img class="img-fluid" src="{{ asset('images/'.$dtContent->image_post)}}" alt="image">
				        
				    </figure>
				   {!! $dtContent->content !!}
			    </div>
				    
			    <nav class="blog-nav nav nav-justified my-5">
                    @if ($page == 1)
                        <a class="nav-link-next nav-item nav-link rounded-right" href="{{route('front.postpage',['page'=>($page+1)])}}">Next<i class="arrow-next fas fa-long-arrow-alt-right"></i></a>
                    @else
                        
                        <a class="nav-link-prev nav-item nav-link rounded-left" href="{{route('front.postpage',['page'=>($page-1)])}}">Previous<i class="arrow-prev fas fa-long-arrow-alt-left"></i></a>
				        <a class="nav-link-next nav-item nav-link rounded-right" href="{{route('front.postpage',['page'=>($page+1)])}}">Next<i class="arrow-next fas fa-long-arrow-alt-right"></i></a>
                    
                    @endif
				  
				</nav>
				
		    </div><!--//container-->
	    </article>
@else
<article class="blog-post px-3 py-5 p-md-5">
		    <div class="container">
                NO DATA 
                <nav class="blog-nav nav nav-justified my-5">
                    <a class="nav-link-prev nav-item nav-link rounded-left" href="{{route('front.postpage',['page'=>($page-1)])}}">Previous<i class="arrow-prev fas fa-long-arrow-alt-left"></i></a>
                </nav>
            </div>
</article>
@endisset	   
@endsection