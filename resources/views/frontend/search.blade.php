@extends('frontend.master.html')
@push('textcss')
    <link href="{{asset('adminlte/plugins/sweetalert2/sweetalert2.css')}}" rel="stylesheet" />
@endpush
@section('content')
        <section class="cta-section theme-bg-light py-5">
		    <div class="container text-center">
			    <h2 class="heading">My Personal Awesome Blog</h2>
                <div class="intro">To get another my article by search</div>
			    <form class="form-inline justify-content-left p-2" method="POST" action="{{route('front.indexsearch')}}" id="formdata">
                    @csrf
                    <table class="table table-sm">  
                    <tr>
                            <td align="left"><strong>Title</strong></td>
                            <td>
                                <input type="text" id="keyword" name="keyword" class="form-control p-2" placeholder="Enter Keyword" value="{{ $dtrequest->keyword }}"/>
                                @if(empty($dtrequest->keyword) && empty($dtrequest->tags) && empty($dtrequest->category))
                                <div class="alert alert-danger">Masukkan Kata Kunci</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Category</strong></td>
                            <td><select class="form-control p-2" name ="category" id="category">
                                    <option value="0" <?php if(!isset($dtrequest->category) || $dtrequest->category == 0){echo 'selected="selected"';} ?>>- category -</option>
                                    @foreach($dtCategory as $key => $dtkat)
                                    <option value="{{$dtkat->id}}" <?php if(isset($dtrequest->category) && $dtrequest->category == $dtkat->id){echo 'selected="selected"';} ?>>{{$dtkat->name}}</option>
                                    @endforeach
                                </select></td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Tags</strong></td>
                            <td><select class="form-control p-2" name ="tags" id="tags">
                                    <option value="0" 
@if (empty($dtrequest->tags) || $dtrequest->tags == 0)
selected="selected"
@endif >- tags -</option>
                                    @foreach($dtTags as $key => $dttags)
                                    <option value="{{$dttags->id}}" 
@if ($dtrequest->tags == $dttags->id)
selected="selected"
@endif >{{$dttags->name}}</option>
                                    @endforeach
                                </select></td>
                        </tr>
                    </table>

                    
                    <button type="submit" class="btn btn-primary" name="submit" id="submit">Search</button>
                </form>
		    </div><!--//container-->
	    </section>
	    <section class="blog-list px-3 py-5 p-md-5">
		    <div class="container">
            @forelse($dtPost as $key => $dtpost)
            @php
            $seconds_ago = (time() - strtotime($dtpost->created_at));
            $teksWaktu="Seen less than a minute ago";
            if ($seconds_ago >= 31536000) {
                $teksWaktu=intval($seconds_ago / 31536000) . " years ago";
            } elseif ($seconds_ago >= 2419200) {
                $teksWaktu=intval($seconds_ago / 2419200) . " months ago";
            } elseif ($seconds_ago >= 86400) {
                $teksWaktu=intval($seconds_ago / 86400) . " days ago";
            } elseif ($seconds_ago >= 3600) {
                $teksWaktu=intval($seconds_ago / 3600) . " hours ago";
            } elseif ($seconds_ago >= 60) {
                $teksWaktu=intval($seconds_ago / 60) . " minutes ago";
            }
            @endphp
                <div class="item mb-5">
				    <div class="media">
					    <img class="mr-3 img-fluid post-thumb d-none d-md-flex" src="{{ asset('images/'.$dtpost->image_post)}}" alt="image">
					    <div class="media-body">
						    <h3 class="title mb-1"><a href="{{ route('front.detail',['slug'=>$dtpost->slug]) }}" target="_blank">{{$dtpost->title}}</a></h3>
						    <div class="meta mb-1"><span class="date">Published {{$teksWaktu}}</span><span class="time">{{$dtpost->view}} view</span><span class="time">Category {{$dtpost->categoryname}}</span></div>
						    <div class="intro">{!! Str::words($dtpost->content,50,'...') !!}</div>
						    <a class="more-link" href="{{ route('front.detail',['slug'=>$dtpost->slug]) }}" target="_blank">Read more &rarr;</a>
					    </div><!--//media-body-->
				    </div><!--//media-->
			    </div><!--//item-->
            @empty
                <div class="item mb-5">
				    <p align="center">tidak ada content</p>
			    </div><!--//item-->
            @endforelse
            
		    </div>
	    </section>
                
@endsection
@push('scripts')
<script src="{{asset('/adminlte/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
<script>

    $('#submit').click(function(event) {
      var formkeyword =  $('#keyword').val();
      var formcategory =  $('#category').val();
      var formtags =  $('#tags').val();
     
      if(formkeyword == '' && parseInt(formcategory) == 0 && parseInt(formtags) == 0)
      {
        event.preventDefault();
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please Input Your Keyword!',
            });
      }
      
    });

</script>
@endpush
