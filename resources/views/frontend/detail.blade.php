@extends('frontend.master.html')
@section('content')
@isset($dtContent->title)
@php
            $seconds_ago = (time() - strtotime($dtContent->created_at));
            $teksWaktu="Seen less than a minute ago";
            if ($seconds_ago >= 31536000) {
                $teksWaktu=intval($seconds_ago / 31536000) . " years ago";
            } elseif ($seconds_ago >= 2419200) {
                $teksWaktu=intval($seconds_ago / 2419200) . " months ago";
            } elseif ($seconds_ago >= 86400) {
                $teksWaktu=intval($seconds_ago / 86400) . " days ago";
            } elseif ($seconds_ago >= 3600) {
                $teksWaktu=intval($seconds_ago / 3600) . " hours ago";
            } elseif ($seconds_ago >= 60) {
                $teksWaktu=intval($seconds_ago / 60) . " minutes ago";
            }
            @endphp
<article class="blog-post px-3 py-5 p-md-5">
		    <div class="container">
			    <header class="blog-post-header">
				    <h2 class="title mb-2">{{$dtContent->title}}</h2>
				    <div class="meta mb-3"><span class="date">Published {{$teksWaktu}}</span><span class="time">{{$dtContent->view}} view</span> <span class="time">Category {{$dtContent->name}}</span></div>
			    </header>
			    
			    <div class="blog-post-body">
				    <figure class="blog-banner">
				        <img class="img-fluid" src="{{ asset('images/'.$dtContent->image_post)}}" alt="image">
				        
				    </figure>
				   {!!$dtContent->content !!}
                   <p>Tags : </p>
                   @forelse ($dtTags as $tagsdata)
                        <ul>{{ $tagsdata->name }}</ul>
                    @empty
                        <ul>No Tags</ul>
                    @endforelse
                    
			    </div>
				
		    </div><!--//container-->
	    </article>
@else
<article class="blog-post px-3 py-5 p-md-5">
		    <div class="container">
                NO DATA 
        </div>
</article>
@endisset	   
@endsection