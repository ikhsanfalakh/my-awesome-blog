<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>{{$dtUser->name}} :: My Awesome Blog</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$dtUser->name}} :: My Personal Blog">
    <meta name="author" content="{{$dtUser->name}}">    
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }} ">
    <meta name="robots" content="noindex, nofollow">
    
    <!-- FontAwesome JS-->
	<script defer src="{{ asset('assets/fontawesome/js/all.min.js') }}"></script>
    
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="{{ asset('assets/css/theme-1.css') }}">
    @stack('textcss')
</head> 

<body>
    <header class="header text-center">	    
	    <h1 class="blog-name pt-lg-4 mb-0"><a href="{{route('front.index')}}"><?php $fnama=explode(' ',$dtUser->name);echo $fnama[0]; ?>'s Blog</a></h1>
        
	    <nav class="navbar navbar-expand-lg navbar-dark" >
           
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

			<div id="navigation" class="collapse navbar-collapse flex-column" >
				<div class="profile-section pt-3 pt-lg-0">
				    <img class="profile-image mb-3 rounded-circle mx-auto" src=" @if ($dtUser->url_foto != '') {{ asset('images/'.$dtUser->url_foto) }} @else {{ asset('assets/images/profile.png') }} @endif " alt="image" >	
					<div class="bio mb-3">{!! $dtUser->intro !!}</div><!--//bio-->
					<ul class="social-list list-inline py-3 mx-auto">
                    @if ($dtUser->url_twitter != '') <li class="list-inline-item"><a href="{{$dtUser->url_twitter}}" target="_blank"><i class="fab fa-twitter fa-fw"></i></a></li> @endif
                    @if ($dtUser->url_linkedin != '') <li class="list-inline-item"><a href="{{$dtUser->url_linkedin}}" target="_blank"><i class="fab fa-linkedin fa-fw"></i></a></li> @endif
                    @if ($dtUser->url_facebook != '') <li class="list-inline-item"><a href="{{$dtUser->url_facebook}}" target="_blank"><i class="fab fa-facebook fa-fw"></i></a></li> @endif
                    @if ($dtUser->url_instagram != '') <li class="list-inline-item"><a href="{{$dtUser->url_instagram}}" target="_blank"><i class="fab fa-instagram fa-fw"></i></a></li> @endif
			        </ul><!--//social-list-->
			        <hr> 
				</div><!--//profile-section-->
				
				<ul class="navbar-nav flex-column text-left">
					<li class="nav-item @if ($leftmenu == 'index') active @endif">
					    <a class="nav-link" href="{{ route('front.index') }}"><i class="fas fa-home fa-fw mr-2"></i>Blog Home </a>
					</li>
					<li class="nav-item @if ($leftmenu == 'post') active @endif">
					    <a class="nav-link" href="{{ route('front.post') }}"><i class="fas fa-bookmark fa-fw mr-2"></i>Blog Post</a>
					</li>
					<li class="nav-item @if ($leftmenu == 'aboutme') active @endif">
					    <a class="nav-link" href="{{ route('front.aboutme') }}"><i class="fas fa-user fa-fw mr-2"></i>About Me</a>
					</li>
				</ul>
                
			</div>
		</nav>
    </header>
    
    <div class="main-wrapper">
        @yield('content')
        
        <footer class="footer text-center py-2 theme-bg-dark">
		   
           <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
           <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i> by <a href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers</small>
          
       </footer>
    </div><!--//main-wrapper-->
    
       
    <!-- Javascript -->          
    <script src="{{ asset('assets/plugins/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/popper.min.js') }}"></script> 
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script> 

    @stack('scripts')

</body>
</html> 