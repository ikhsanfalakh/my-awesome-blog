@extends('layout.master')

@push('textcss')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css') }}"/>
    <style>
        .select2-container--default
        .select2-selection--multiple
        .select2-selection__rendered
        .select2-selection__choice {
            color: #444;
        }
    </style>
@endpush

@section('content')
<div class="container">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Tags</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <!-- form start -->
                    <form class="form-horizontal" action="{{ route('back.tagstore')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="name">Name Tag</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Tag" value="{{ old('name', '') }}">
                                @error('name')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.card -->

        </div>
        <!--/.col (left) -->
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/select2/js/select2.min.js')}}"></script>

<!-- Page specific script -->
<script>
    $(function() {
        // Summernote
        $('#content').summernote({
                height: 300,   //set editable area's height
            })
    });

    $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.js-example-basic-multiple').select2();
    });
</script>
@endpush
