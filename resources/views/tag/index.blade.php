@extends('layout.master')

@push('textcss')
    <link href="{{asset('adminlte/plugins/sweetalert2/sweetalert2.css')}}" rel="stylesheet" />
@endpush

@section('content')
<section class="content">
    <!-- Default box -->
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Data Tags</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <table id="table_tags" class="table table-bordered table-striped">
            <a href="/admin/tag/create" class="btn btn-primary">Tambah</a>
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th style="width: 20%" scope="col">Name</th>
                    <th style="width: 18%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($tag as $index=>$value)
                <tr>
                    <td>{{$index}}</th>
                    <td>{{$value->name}}</td>
                    <td>
                        <form action="/admin/tag/{{$value->id}}" method="POST">
                            <a href="/admin/tag/{{$value->id}}" class="btn btn-info"><i class="fas fa-file-alt"></i></a>
                            <a href="/admin/tag/{{$value->id}}/edit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>

                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger my-1 delete-confirm">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="8" align="center">No data</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th scope="col">#</th>
                    <th>Name</th>
                    <th scope="col">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
</section>
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('/adminlte/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
<script>
    $(function() {
        $("#table_tags").DataTable();
    });

    $('.delete-confirm').click(function(event) {
      var form =  $(this).closest("form");
      var name = $(this).data("name");
      event.preventDefault();
      swal.fire({
            title: 'Are you sure?',
            text: 'This record and it`s details will be permanantly deleted!',
            icon: 'warning',
            title: 'Are you sure?',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
      .then((result) => {
  		if (result.isConfirmed) {
          form.submit();
        }
      });
  });

</script>
@endpush
