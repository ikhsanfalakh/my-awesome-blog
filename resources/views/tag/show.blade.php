@extends('layout.master')

@section('content')

<section class="content">
    <!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Tags</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <h2>Tampilkan Tag ID {{$tag->id}}</h2>
        <h4>{{$tag->name}}</h4>
    </div>
    <!-- /.card-body -->
</div>
</section>
@endsection
