<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tag extends Model
{
    protected $table = 'tags';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function post()
    {
        return $this->hasMany('App\PostTag');
    }

    public function getTags($idTags)
    {
        if(is_numeric($idTags) && $idTags > 0)
        {
            return DB::table('tags')->where('id', $idTags)->first();
        }
        else
        {
            return DB::table('tags')->get();
        }
        
    }
}
