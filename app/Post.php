<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Post extends Model
{
    public function Category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getPost($page)
    {
        $offset=($page-1)*5;
        return DB::table('posts')->join('category', 'posts.category_id', '=', 'category.id')->select('posts.id','posts.title','posts.slug','posts.image_post','posts.view','posts.created_at','posts.content','category.name')->offset($offset)->limit(5)->get();
    }

    public function getPostLatest($page)
    {
        if(is_numeric($page) && $page > 1)
        {
            $offset=$page-1;
            return DB::table('posts')->join('category', 'posts.category_id', '=', 'category.id')->select('posts.id','posts.title','posts.slug','posts.image_post','posts.view','posts.created_at','posts.content','category.name')->offset($offset)->latest()->first();
        }
        else
        {
            return DB::table('posts')->join('category', 'posts.category_id', '=', 'category.id')->select('posts.id','posts.title','posts.slug','posts.image_post','posts.view','posts.created_at','posts.content','category.name')->latest()->first();
        }
        
    }

    public function getPostsBySluq($slug)
    {
        return DB::table('posts')->join('category', 'posts.category_id', '=', 'category.id')->select('posts.id','posts.title','posts.slug','posts.image_post','posts.view','posts.created_at','posts.content','category.name')->where('posts.slug', $slug)->first();
    }

    public function getSearchPost($request)
    {
        // search kosong maka skip
        $dataReturn=array();
        if(!empty($request->keyword))
        {
            $queryWhere='(posts.title like \'%'.$request->keyword.'%\' OR posts.content like \'%'.$request->keyword.'%\')';
            if(is_numeric($request->category) && $request->category > 0)
            {
                $queryWhere.=' AND posts.category_id='.$request->category;
            }
            if(is_numeric($request->tags) && $request->tags > 0)
            {
                $queryWhere.=' AND posts_tags.tag_id='.$request->tags;
            }
            
            $dataReturn=DB::table('posts')
                ->join('category', 'posts.category_id', '=', 'category.id')
                ->join('posts_tags', 'posts.id', '=', 'posts_tags.post_id')
                ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
                ->select('posts.*', 'tags.name','category.name as categoryname')
                ->whereRaw($queryWhere)->get();
            
        }
        elseif(isset($request->tags) && isset($request->category) && is_numeric($request->category) && $request->category > 0 && is_numeric($request->tags) && $request->tags > 0)
        {
            $dataReturn=DB::table('posts')
                ->join('category', 'posts.category_id', '=', 'category.id')
                ->join('posts_tags', 'posts.id', '=', 'posts_tags.post_id')
                ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
                ->select('posts.*', 'tags.name','category.name as categoryname')
                ->where([['posts.category_id', '=', $request->category],
                    ['posts_tags.tag_id', '=', $request->tags]])->get();
        }
        elseif(isset($request->tags) && is_numeric($request->tags) && $request->tags > 0)
        {
            $dataReturn=DB::table('posts')
                ->join('category', 'posts.category_id', '=', 'category.id')
                ->join('posts_tags', 'posts.id', '=', 'posts_tags.post_id')
                ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
                ->select('posts.*', 'tags.name','category.name as categoryname')
                ->where('posts_tags.tag_id',$request->tags)->get();
        }
        elseif(isset($request->category) && is_numeric($request->category) && $request->category > 0)
        {
            $dataReturn=DB::table('posts')
                ->join('category', 'posts.category_id', '=', 'category.id')
                ->select('posts.*', 'category.name as categoryname')
                ->where('posts.category_id',$request->category)->get();
        }
        
        return $dataReturn;
    }
    public function incView($slug)
    {
        // menaikkan 1 angka view
        return DB::table('posts')
        ->where('slug',$slug)
        ->update(['view'=> ((DB::table('posts')->select('view')->where('slug',$slug)->first()->view)+1)]); 
    }
}
