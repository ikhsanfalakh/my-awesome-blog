<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function post()
    {
        return $this->hasMany('App\Post','id','id');
    }

    public function getCategory($idCat)
    {
        if(is_numeric($idCat) && $idCat > 0)
        {
            return DB::table('category')->where('id', $idCat)->first();
        }
        else
        {
            return DB::table('category')->get();
        }
    }
}
