<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index_old()
    {
        $user=new User;
        return view('profile.index',[
            'user' => $user->getUser(),
        ]);
    }
    public function index()
    {
        $user=new User;
        return view('profile.index',[
            'user' => $user->getUser(),
        ]);
    }
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $upload_path = public_path('images/');
        if (!empty($request['foto'])) {
            $foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request['foto']));

            $extension = (explode('/', mime_content_type($request['foto']))[1]);
            $foto_name = 'profile/'.strtoupper(date('ymd').uniqid()).'.'.$extension;

            file_put_contents($upload_path.$foto_name, $foto);
        }

        $data_user = User::find(1);
        $data_user->name = $request['name'];
        $data_user->email = $request['email'];
        $data_user->desc = $request['desc'];
        $data_user->url_instagram = $request['url_instagram'];
        $data_user->url_facebook = $request['url_facebook'];
        $data_user->url_website = $request['url_website'];
        $data_user->url_twitter = $request['url_twitter'];
        $data_user->url_linkedin = $request['url_linkedin'];
        if(!empty($request['foto'])){
            $data_user->url_foto = $foto_name;
        }
        $data_user->intro = $request['intro'];
        $data_user->save();

        $user=new User;
        return view('profile.index',[
            'user' => $user->getUser(),
        ])->with('success','Berhasil disimpan');
    }

    public function showChangePasswordForm(Request $request)
    {
        $user=new User;
        return view('auth.passwords.change',[
            'user' => $user->getUser(),
        ]);
    }

    public function changePassword(Request $request)
    {
        //dd($request);
        $result = true;
        if (!(Hash::check($request['current-password'], Auth::user()->password))) {
            // The passwords matches
            $result = false;
            return redirect()->back()->with("error","Password sekarang tidak sesuai.");
        }
        if(strcmp($request['current-password'], $request['new-password']) == 0){
            //Current password and new password are same
            $result = false;
            return redirect()->back()->with("error","Password baru sama dengan password lama. Silahkan ganti password yang berbeda.");
        }
        if(!(strcmp($request['new-password'], $request['new-password-confirm'])) == 0){
            //New password and confirm password are not same
            $result = false;
            return redirect()->back()->with("error","Password baru harus sama dengan konfirmasi password.");
        }

        if($result) {
            $user = Auth::user();
            $user->password = bcrypt($request['new-password']);
            $user->save();
        }

        return redirect()->back()->with("success","Berhasil disimpan");
    }
}
