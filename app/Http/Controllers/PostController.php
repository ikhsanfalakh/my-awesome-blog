<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Category;
use App\PostTag;
use App\Tag;
use DB;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user=new User;
        $artikel=new Post;
        $dataArtikel=[];
        $dtArticle=$artikel->all();
        if(count($dtArticle) > 0)
        {
            foreach($dtArticle as $dtArticel)
            {
                $dataTags=DB::table('posts_tags')
            ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
            ->select('tags.name')
            ->where('posts_tags.post_id',$dtArticel->id)->get();
                $dataArtikel[]=array($dtArticel,$dataTags);
            }
        }
        return view('post.index',[
            'user' => $user->getUser(),
            'artikel' => $dataArtikel,
        ]);
    }

    public function create()
    {
        $user = new User;
        $listcategory = new Category;
        $listtag = new Tag;
        return view('post.create',[
            'user' => $user->getUser(),
            'listcategory' => $listcategory->all(),
            'listtag' => $listtag->all(),
        ]);
    }

    public function store(Request $request)
    {
        //dd($request->foto->hashName());
        $request->validate([
            'title' => 'required',
        ]);

        $upload_path = public_path('images/');
        if (!empty($request['foto'])) {
            $foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request['foto']));

            $extension = (explode('/', mime_content_type($request['foto']))[1]);
            $foto_name = 'post/'.strtoupper(date('ymd').uniqid()).'.'.$extension;

            file_put_contents($upload_path.$foto_name, $foto);
        }

        $auth = Auth::user();
        DB::beginTransaction();
        try {
            $data_post = new Post;
            $data_post->title = $request['title'];
            $data_post->slug = $request['slug'];
            $data_post->content = $request['content'];
            $data_post->category_id = $request['category_id'];
            if(!empty($request['foto'])){
                $data_post->image_post = $foto_name;
            }
            $data_post->user_id = $auth->id;
            $data_post->save();

            $post_id = DB::getPdo()->lastInsertId();

            /* foreach ($request['tag_id'] as $value) {
                $query = DB::table('posts_tags')->insert([
                    'post_id' => $post_id,
                    'tag_id' => $value
                ]);
            } */
            foreach ($request['tag_id'] as $key=>$value) {
                $datatags[] = array(
                                'post_id' => $post_id,
                                'tag_id' => $value
                );
            }
            PostTag::insert($datatags);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        $user=new User;
        $artikel=new Post;
        $dataArtikel=[];
        $dtArticle=$artikel->all();
        if(count($dtArticle) > 0)
        {
            foreach($dtArticle as $dtArticel)
            {
                $dataTags=DB::table('posts_tags')
            ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
            ->select('tags.name')
            ->where('posts_tags.post_id',$dtArticel->id)->get();
                $dataArtikel[]=array($dtArticel,$dataTags);
            }
        }
        return view('post.index',[
            'user' => $user->getUser(),
            'artikel' => $dataArtikel,
        ]);
    }

    public function show($id)
    {
        $user = new User;
        $dataPost=DB::table('posts')
                ->join('category', 'posts.category_id', '=', 'category.id')
                ->select('posts.*', 'category.name as categoryname')
                ->where('posts.id',$id)->first();

        $dataTags=DB::table('posts_tags')
            ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
            ->select('tags.name')
            ->where('posts_tags.post_id',$id)->get();

        return view('post.show', [
            'user' => $user->getUser(),
            'artikel'=>$dataPost,
            'listtag'=>$dataTags
        ]);
    }

    public function edit($id)
    {
        $user = new User;
        $artikel = new Post;
        $listcategory = new Category;
        $listtag = new Tag;
        $listposttag = new PostTag;

        return view('post.edit',[
            'user' => $user->getUser(),
            'artikel' => $artikel->find($id),
            'listpoststags' => $listposttag->getTagId($id),
            'listcategory' => $listcategory->all(),
            'listtag' => $listtag->all(),
        ]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $upload_path = public_path('images/');
        if (!empty($request['foto'])) {
            $foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request['foto']));

            $extension = (explode('/', mime_content_type($request['foto']))[1]);
            $foto_name = 'post/'.strtoupper(date('ymd').uniqid()).'.'.$extension;

            file_put_contents($upload_path.$foto_name, $foto);
        }

        $auth = Auth::user();
        DB::beginTransaction();
        try {
            $data_post = Post::find($id);
            $data_post->title = $request['title'];
            $data_post->slug = $request['slug'];
            $data_post->content = $request['content'];
            $data_post->category_id = $request['category_id'];
            if(!empty($request['foto'])){
                $data_post->image_post = $foto_name;
            }
            $data_post->user_id = $auth->id;
            $data_post->save();

            $post_id = $id;

            /* foreach ($request['tag_id'] as $value) {
                $query = DB::table('posts_tags')->insert([
                    'post_id' => $post_id,
                    'tag_id' => $value
                ]);
            } */
            $query = DB::table('posts_tags')->where('post_id', $id)->delete();
            if(!empty($request['tag_id'])) {
                foreach ($request['tag_id'] as $key=>$value) {
                    $datatags[] = array(
                                    'post_id' => $post_id,
                                    'tag_id' => $value
                    );
                }
                PostTag::insert($datatags);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        $user=new User;
        $artikel=new Post;
        $dataArtikel=[];
        $dtArticle=$artikel->all();
        if(count($dtArticle) > 0)
        {
            foreach($dtArticle as $dtArticel)
            {
                $dataTags=DB::table('posts_tags')
            ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
            ->select('tags.name')
            ->where('posts_tags.post_id',$dtArticel->id)->get();
                $dataArtikel[]=array($dtArticel,$dataTags);
            }
        }
        return view('post.index',[
            'user' => $user->getUser(),
            'artikel' => $dataArtikel,
        ]);
    }

    public function destroy($id)
    {
        DB::table('posts_tags')->where('post_id', $id)->delete();
        Post::find($id)->delete();
        return back()->with('success','Data Berhasil dihapus');
    }
}
