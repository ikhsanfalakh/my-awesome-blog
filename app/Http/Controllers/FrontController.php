<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Post;
use App\Category;
use App\User;
use App\PostTag;
use Illuminate\Support\Str;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cFrontUser=new User;
        $cFrontTag=new Tag;
        $cFrontPost=new Post;
        $cFrontCategory=new Category;
        return view('frontend.index',[
            'leftmenu'=>'index',
            'dtUser' => $cFrontUser->getUser(),
            'dtPost'=>$cFrontPost->getPost(1),
            'dtCategory'=>$cFrontCategory->getCategory(0),
            'dtTags'=>$cFrontTag->getTags(0),
            'page'=>1
            ]);
    }

    /**
     * Menampilkan list blog
     *
     * @return \Illuminate\Http\Response
     */
    public function post()
    {
        $cFrontUser=new User;
        $cFrontPost=new Post;
        
        return view('frontend.post',['leftmenu'=>'post','dtUser' => $cFrontUser->getUser(),'dtContent'=>$cFrontPost->getPostLatest(1),'page'=>1]);
    }
    /**
     * Menampilkan list blog per paging
     *
     * @return \Illuminate\Http\Response
     */
    public function postpage($page)
    {
        $cFrontUser=new User;
        $cFrontPost=new Post;
        $dtContent=$cFrontPost->getPostLatest($page);
        if(isset($dtContent->slug))
        {
            $hasilUpdateView=$cFrontPost->incView($dtContent->slug);
        }
        return view('frontend.post',['leftmenu'=>'post','dtUser' => $cFrontUser->getUser(),'dtContent'=>$dtContent,'page'=>$page]);
    }
    /**
     * Menampilkan About Me
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutme()
    {
        $cFrontUser=new User;
        return view('frontend.aboutme',['leftmenu'=>'aboutme','dtUser' => $cFrontUser->getUser()]);
    }

    /**
     * Menampilkan hasil search
     *
     * @return \Illuminate\Http\Response
     */
    public function cari(Request $request)
    {
        $cFrontUser=new User;
        $cFrontTag=new Tag;
        $cFrontPost=new Post;
        $cFrontCategory=new Category;
        
        return view('frontend.search',['leftmenu'=>'index','dtUser' => $cFrontUser->getUser(),
        'dtPost'=>$cFrontPost->getSearchPost($request),
        'dtCategory'=>$cFrontCategory->getCategory(0),
        'dtTags'=>$cFrontTag->getTags(0),
        'dtrequest'=>$request]);
        
    }

    /**
     * Menampilkan hasil search
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($slug)
    {
        $cFrontUser=new User;
        $cFrontPost=new Post;
        $cFrontPostTags=new PostTag;
        
        $hasilUpdateView=$cFrontPost->incView($slug);
        $dtContent=$cFrontPost->getPostsBySluq($slug);
        $dataTags=array();
        if(isset($dtContent->id))
        {
            $dataTags=$cFrontPostTags->getPostTagByIdPost($dtContent->id);
        }
        return view('frontend.detail',['leftmenu'=>'post','dtUser' => $cFrontUser->getUser(),'dtContent'=>$dtContent,'dtTags'=>$dataTags]);
    }

    /**
     * Menampilkan list blog
     *
     * @return \Illuminate\Http\Response
     */
    public function next($page)
    {
        $cFrontUser=new User;
        $cFrontTag=new Tag;
        $cFrontPost=new Post;
        $cFrontCategory=new Category;
        return view('frontend.index',[
            'leftmenu'=>'index',
            'dtUser' => $cFrontUser->getUser(),
            'dtPost'=>$cFrontPost->getPost($page),
            'dtCategory'=>$cFrontCategory->getCategory(0),
            'dtTags'=>$cFrontTag->getTags(0),
            'page'=>$page,
            ]);
    }
}

