<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\User;
use Illuminate\Support\Str;
use DB;

class CategoryController extends Controller
{
    public function index() {
        $category = DB::table('category')->get();
        $user = User::where('id',1)->first();
        //dd($user);
        //return view('category.index', ['category' => $category,
                                        //'user' => $user]);
        return view('category.index', compact('user', 'category'));
    }

    public function create()
    {
        $category = DB::table('category')->get();
        $user = User::where('id',1)->first();
        return view('category.create', compact('user', 'category'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:category',
        ]);
        $query = DB::table('category')->insert([
            "name" => $request["name"]
        ]);
        return redirect('/admin/category');
    }

    public function show($id)
    {
        $category = DB::table('category')->where('id', $id)->first();
        $user = User::where('id',1)->first();
        return view('category.show', compact('user', 'category'));
    }

    public function edit($id){
        $category=Category::where('id', $id)->first();
        $user = User::where('id',1)->first();
        //return view('category.edit', ['data'=>$idcategory]);
        //dd($idcategory);
        return view('category.edit', compact('user' ,'category'));
    }

    public function update(Request $request, $id){
        $idcategory=Category::where('id', $id)->first();
        $idcategory->name = $request->category;
        $idcategory->save();

        $category=Category::all();
        $user = User::where('id',1)->first();
        return view('category.index', compact('user','category'));
    }

    public function destroy($id){

        $query = DB::table('category')->where('id', $id)->delete();
        return redirect('/admin/category');

        /*$idcategory=Category::where('id', $id)->first();
        $idcategory->destroy();

        $category=Category::all();
        $user = User::where('id',1)->first();
        return view('category.index', compact('user','category'));*/
    }
}
