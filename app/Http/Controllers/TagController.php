<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\User;
use Illuminate\Support\Str;
use DB;

class TagController extends Controller
{
    public function index() {
        $tag = DB::table('tags')->get();
        $user = User::where('id',1)->first();
        //dd($user);
        //return view('category.index', ['category' => $category,
                                        //'user' => $user]);
        return view('tag.index', compact('user', 'tag'));
    }

    public function create()
    {
        $tag = DB::table('tags')->get();
        $user = User::where('id',1)->first();
        return view('tag.create', compact('user', 'tag'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:tags',
        ]);
        $query = DB::table('tags')->insert([
            "name" => $request["name"]
        ]);
        return redirect('/admin/tag');
    }

    public function show($id)
    {
        $tag = DB::table('tags')->where('id', $id)->first();
        $user = User::where('id',1)->first();
        return view('tag.show', compact('user', 'tag'));
    }

    public function edit($id){
        $tag=Tag::where('id', $id)->first();
        $user = User::where('id',1)->first();
        //return view('category.edit', ['data'=>$idcategory]);
        //dd($idcategory);
        return view('tag.edit', compact('user' ,'tag'));
    }

    public function update(Request $request, $id){
        $idtag=Tag::where('id', $id)->first();
        $idtag->name = $request->tag;
        $idtag->save();

        $tag=Tag::all();
        $user = User::where('id',1)->first();
        return view('tag.index', compact('user','tag'));
    }

    public function destroy($id){

        $query = DB::table('tags')->where('id', $id)->delete();
        return redirect('/admin/tag');

        /*$idcategory=Category::where('id', $id)->first();
        $idcategory->destroy();

        $category=Category::all();
        $user = User::where('id',1)->first();
        return view('category.index', compact('user','category'));*/
    }
}
