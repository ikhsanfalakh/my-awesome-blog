<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use DB;

class PostTag extends Model
{
    protected $table = 'posts_tags';
    protected $primaryKey = ['post_id','tag_id'];
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

    public function post()
    {
        return $this->hasMany('App\Post','id','post_id');
    }

    public function tag()
    {
        return $this->hasMany('App\Tag','id','tag_id');
    }

    public function getPostTagByIdPost($idpost)
    {
        return DB::table('posts_tags')
                ->join('tags', 'posts_tags.tag_id', '=', 'tags.id')
                ->select('tags.*')
                ->where('posts_tags.post_id',$idpost)->get();
    }

    public function getTagId($idpost)
    {
        return DB::table('posts_tags')
                ->where('post_id', $idpost)
                ->get();
    }
}
