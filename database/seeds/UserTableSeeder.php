<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Nama Anggota',
            'email' => 'user@email.com',
            'password' => bcrypt('123456'),
            'desc'=>'',
            'url_instagram'=>'',
            'url_facebook'=>'',
            'url_website'=>'',
            'url_twitter'=>'',
            'url_linkedin'=>'',
            'url_foto'=>'',
            'intro'=>'',
        ]);
    }
}
