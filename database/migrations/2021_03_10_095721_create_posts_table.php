<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        CREATE TABLE IF NOT EXISTS `mydb`.`posts` (
        `id` BIGINT NOT NULL AUTO_INCREMENT,
        `title` VARCHAR(45) NULL,
        `slug` VARCHAR(45) NULL,
        `content` TEXT NULL,
        `category_id` INT NULL,
        `image_post` VARCHAR(255) NULL,
        `view` INT NULL,
        `user_id` INT NULL,
        `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
        `update_time` TIMESTAMP NULL,
        PRIMARY KEY (`id`),
        INDEX `fk_posts_category_idx` (`category_id` ASC) VISIBLE,
        INDEX `fk_posts_user_id_idx` (`user_id` ASC) VISIBLE,
        CONSTRAINT `fk_posts_category_id`
            FOREIGN KEY (`category_id`)
            REFERENCES `mydb`.`category` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
        CONSTRAINT `fk_posts_user_id`
            FOREIGN KEY (`user_id`)
            REFERENCES `mydb`.`users` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);
        */
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',100);
            $table->string('slug',100);
            $table->text('content');
            $table->integer('category_id')->unsigned();
            $table->string('image_post',255);
            $table->integer('view')->unsigned()->default(0);
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('category');
            $table->foreign('user_id')->references('id')->on('users');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
