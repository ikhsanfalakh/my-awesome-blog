<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDefaultUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                $table->text('desc')->nullable()->change();
                $table->string('url_instagram',100)->nullable()->change();
                $table->string('url_facebook',100)->nullable()->change();
                $table->string('url_website',100)->nullable()->change();
                $table->string('url_twitter',100)->nullable()->change();
                $table->string('url_linkedin',100)->nullable()->change();
                $table->string('url_foto',255)->nullable()->change();
                $table->string('intro',250)->nullable()->change();
                
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
