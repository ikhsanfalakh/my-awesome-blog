<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('front.index');
Route::post('/', 'FrontController@cari')->name('front.indexsearch');
Route::get('/post', 'FrontController@post')->name('front.post');
Route::get('/post/page/{page}', 'FrontController@postpage')->name('front.postpage');
Route::get('/aboutme', 'FrontController@aboutme')->name('front.aboutme');
Route::get('/detail/{slug}', 'FrontController@detail')->name('front.detail');
Route::get('/tags/{slug}', 'FrontController@tags')->name('front.tags');
Route::get('/category/{category}', 'FrontController@category')->name('front.category');
Route::get('/page/{page}', 'FrontController@next')->name('front.page');

Auth::routes();

Route::get('admin/category/create', 'CategoryController@create')->name('back.categorycreate');
Route::post('admin/category/create', 'CategoryController@store')->name('back.categorystore');
Route::get('admin/category', 'CategoryController@index')->name('back.categoryindex');;
Route::get('admin/category/{category_id}', 'CategoryController@show');
Route::get('admin/category/{category_id}/edit', 'CategoryController@edit');
Route::put('admin/category/{category_id}', 'CategoryController@update');
Route::delete('admin/category/{category_id}', 'CategoryController@destroy');

Route::get('admin/tag/create', 'TagController@create')->name('back.tagcreate');
Route::post('admin/tag/create', 'TagController@store')->name('back.tagstore');
Route::get('admin/tag', 'TagController@index')->name('back.tagindex');;
Route::get('admin/tag/{tag_id}', 'TagController@show');
Route::get('admin/tag/{tag_id}/edit', 'TagController@edit');
Route::put('admin/tag/{tag_id}', 'TagController@update');
Route::delete('admin/tag/{tag_id}', 'TagController@destroy');

Route::get('/admin', 'HomeController@index')->name('home');
Route::get('/admin/profile', 'ProfileController@index');
Route::put('/admin/profile/{id}', 'ProfileController@update');

Route::get('/admin/gantipassword','ProfileController@showChangePasswordForm');
Route::post('/admin/gantipassword','ProfileController@changePassword')->name('gantipassword');

Route::get('/admin/artikel', 'PostController@index');
Route::get('/admin/artikel/create', 'PostController@create');
Route::post('/admin/artikel', 'PostController@store');
Route::get('/admin/artikel/{artikel_id}', 'PostController@show');
Route::get('/admin/artikel/{artikel_id}/edit', 'PostController@edit');
Route::put('/admin/artikel/{artikel_id}', 'PostController@update');
Route::delete('/admin/artikel/{artikel_id}', 'PostController@destroy');
