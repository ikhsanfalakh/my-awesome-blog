# My Awesome Blog

## Final Kelompok 20 - Sanbercode Batch 22

## Nama Kelompok :
- Mukhammad Ikhsan Falakh ([@ikhsanfalakh](https://gitlab.com/ikhsanfalakh))
- Tomy Dwi Dayanto ([@chubee12002](https://gitlab.com/tomytjoen))
- Willy Rayadi ([@WillyRayadi](https://gitlab.com/Willy_Rayadi))

## Tema : **My Awesome Blog**

## Link Video Demo
[DEMO FRONT PAGE](https://drive.google.com/drive/folders/1orvnSXC8uEWtXyQxfTsHdcoSI8aEIQT1?usp=sharing)

## Link Deploy Demo
- [DEMO FRONT PAGE](http://my-awesome-blog.getxcafe.com)
- [DEMO ADMIN](http://my-awesome-blog.getxcafe.com/admin)\
User Email : user@email.com\
Password : 123456

## ERD
![picture alt](public/images/ERD.png "ERD")

## UI/UX template
- Front Page : [DevBlog](https://themes.3rdwavemedia.com/demo/devblog/index.html)
- Admin Page : [AdminLTE 3](https://adminlte.io/)

## Library/Packages
- Summernote
- Select2
- Datatables
- SweetAlert2
- Croppie

## Fitur
A. Front Page
1. Home Page
2. Detail Artikel
3. Artikel berdasarkan Tags
4. Artikel berdasarkan Kategori
5. Pencarian

B. Admin Page
1. Login/Logout
2. Update Profil
3. CRUD Kategori
4. CRUD Tags
5. CRUD Artikel
6. Ganti Password
